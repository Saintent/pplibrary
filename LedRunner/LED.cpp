/*
 * LED.cpp
 *
 *  Created on: Oct 1, 2016
 *      Author: Prustya
 */

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "LED.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PROGRAMMING DEFINITION INCLUDE ----------------------------------------------------- //
// N/A
// ---------- EXTERN OBJECT ---------------------------------------------------------------------- //
// N/A
// ---------- PUBLIC INTERFACE METHOD ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC METHOD (FUNCTION PROTOTYPE) ------------------------------------------------- //
// N/A
// ---------- PUBLIC DATA ----------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE METHOD (FUNCTION PROTOTYPE) ----------------------------------------------- //
// N/A
// ---------- PRIVATE DATA ---------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE PROGRAMMING DEFINE -------------------------------------------------------- //
// N/A
// ---------- PRIVATE MACRO DEFINITION ---------------------------------------------------------- //
// N/A
// ---------- SOURCE FILE IMPLEMENTATION -------------------------------------------------------- //
/**\brief Class constructor
 *
 */
LED::LED() {

	Init(0, blinkTime, PIO::Stay);
}

/** \brief Class constructor with parameter
 * 	\param pin Led output pin
 * 	\param blinkTime LED blink time
 * 	\param mode Output mode
 *
 */
LED::LED(uint8_t pin, uint32_t blinkTime, PIO::IOOutputMode mode) {

	Init(pin, blinkTime, mode);
}

/**	\brief Class destructor
 *
 */
LED::~LED() {

}

/** \brief LED initialized
 * 	\param pin Led output pin
 * 	\param blinkTime LED blink time
 * 	\param mode Output mode
 *
 */
void LED::Init(uint8_t pin, uint32_t blinkTime, PIO::IOOutputMode mode) {
	this->pin = pin;
	this->state = PIO::Low;
	this->blinkTime = blinkTime;
	this->outPutMode = mode;
	this->Mode(PIO::Output);
	timeCounter = 0;
	activated = Led::LedDisactivated;
	ledStatus = Led::LedOff;
}

/**	\brief LED on function */
void LED::On() {
	this->ledStatus = Led::LedOn;
	this->state = PIO::High;
	Write(PIO::High);
}

/**	\brief LED off function */
void LED::Off() {
	this->ledStatus = Led::LedOff;
	this->state = PIO::Low;
	Write(PIO::Low);
}

/**	\brief LED Toggle function */
void LED::Toggle() {
	if (this->ledStatus == Led::LedOn) {
		this->Off();
	}
	else {
		this->On();
	}
}

/** \brief Run function
 * 	process depend on current mode.
 * 		Toggle mode LED will toggle depend on blinkTime parameter
 * 		OneShot mode LED will on until meet blinkTime then LED will off
 */
void LED::Run() {
	if (this->activated == Led::LedActivated) {
		if (++this->timeCounter >= this->blinkTime) {
			// Execute this led
			switch (this->outPutMode) {
			case PIO::Stay:
				// Do not thing
				break;
			case PIO::Toggle:
				this->Toggle();
				break;
			case PIO::OneShot:
				this->Off();
				this->activated = Led::LedDisactivated;
				break;
			case PIO::PWM:
				//Not support now
				break;
			}
			this->timeCounter = 0;
		}
	}
}

void LED::Activated(void) {
	this->activated = Led::LedActivated;
	this->timeCounter = 0;
}

void LED::Disactivated(void) {
	this->activated = Led::LedDisactivated;
}

void LED::OneShot(uint32_t period) {
	this->blinkTime = period;
	this->outPutMode = PIO::OneShot;
	this->On();
	this->Activated();
}

void LED::SetMode(PIO::IOOutputMode outPutMode, uint32_t period) {
	this->blinkTime = period;
	this->outPutMode = outPutMode;
}

//=========== Public Method ======================================================================//

//=========== Private Method ======================================================================//

// ---------- END OF SOURCE FILE IMPLEMENTATION ------------------------------------------------- //
