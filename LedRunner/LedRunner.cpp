/*
 * LedRunner.cpp
 *
 *  Created on: Oct 1, 2016
 *      Author: Prustya
 */

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "LedRunner.h"
#include "NodeFuntion.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PROGRAMMING DEFINITION INCLUDE ----------------------------------------------------- //
// N/A
// ---------- EXTERN OBJECT ---------------------------------------------------------------------- //
// N/A
// ---------- PUBLIC INTERFACE METHOD ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC METHOD (FUNCTION PROTOTYPE) ------------------------------------------------- //
// N/A
// ---------- PUBLIC DATA ----------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE METHOD (FUNCTION PROTOTYPE) ----------------------------------------------- //
// N/A
// ---------- PRIVATE DATA ---------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE PROGRAMMING DEFINE -------------------------------------------------------- //
// N/A
// ---------- PRIVATE MACRO DEFINITION ---------------------------------------------------------- //
// N/A
// ---------- SOURCE FILE IMPLEMENTATION -------------------------------------------------------- //


LedRunner::LedRunner() {
	// TODO Auto-generated constructor stub
	head = 0;
	ledCount = 0;
}

LedRunner::~LedRunner() {
	// TODO Auto-generated destructor stub
}

uint32_t LedRunner::AddLED(LED* pLED) {

	ledCount++;
	return NodeAdd<LED>(pLED, &head);

}

uint32_t LedRunner::AddLED(uint8_t pin, uint32_t blinkTime, PIO::IOOutputMode mode) {
	LED* newLed;

	newLed = new LED(pin, blinkTime, mode);
	ledCount++;
	return NodeAdd<LED>(newLed, &head);
}

Node<LED>* LedRunner::GetHead() {
	return head;
}

void LedRunner::Run() {
	Node<LED>* node;
	LED* led;
	node = head;
	uint8_t writeValue;

	while(node) {
		node->data->Run();
		/*led = node->data;
		led->Run();*/
		node = node->NextNode();
		/*if (led->activated == Led::LedActivated) {
			if (++led->timeCounter >= led->blinkTime) {
				// Execute this led
				switch (led->outPutMode) {
				case PIO::Stay:
					break;
				case PIO::Toggle:
					led->Toggle();
					break;
				case PIO::OneShot:
					led->Off();
					break;
				case PIO::PWM:
					break;
				}
				led->timeCounter = 0;
			}
		}*/


	}
}

void LedRunner::CallBack(void* obj) {
	LedRunner* ledRunner;
	ledRunner = (LedRunner*) obj;
	ledRunner->Run();
}

//=========== Public Method ======================================================================//

//=========== Private Method ======================================================================//

// ---------- END OF SOURCE FILE IMPLEMENTATION ------------------------------------------------- //
