/*
 * LED.h
 *
 *  Created on: Oct 1, 2016
 *      Author: Prustya
 */

#ifndef LED_H_
#define LED_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
#include "IOBase.h"
#include "AIO.h"
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
//
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
namespace Led {
	//!< Led process status
	enum LedProcessStatus {
		LedDisactivated,	//!< Process was stop
		LedActivated		//!< Process is running
	};

	//!< Led status
	enum LedStatus {
		LedOn,				//!< LED is on
		LedOff				//!< LED is off
	};
}
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/* \brief 	LED class
 * 			Handle one LED
 *
 */
class LED : public AIO {
public:

	LED();
	LED(uint8_t pin, uint32_t blinkTime, PIO::IOOutputMode mode = PIO::Stay);
	virtual ~LED();

	void Init(uint8_t pin, uint32_t blinkTime, PIO::IOOutputMode mode);

	void On();
	void Off();
	void Toggle();

	void Run();

	void Activated(void);
	void Disactivated(void);

	void OneShot(uint32_t period);
	void SetMode(PIO::IOOutputMode outPutMode, uint32_t period);



private:
	Led::LedStatus ledStatus;
	Led::LedProcessStatus activated;

	PIO::IOOutputMode outPutMode;
	uint32_t blinkTime;
	uint32_t timeCounter;

};

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* LED_H_ */
