/*
 * LedRunner.h
 *
 *  Created on: Oct 1, 2016
 *      Author: Prustya
 */

#ifndef LEDRUNNER_H_
#define LEDRUNNER_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
#include "IOBase.h"
#include "Node.h"
#include "LED.h"
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
//
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
//
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/*
 *
 */
class LedRunner {
public:
	LedRunner();
	virtual ~LedRunner();

	uint32_t AddLED(uint8_t pin, uint32_t blinkTime, PIO::IOOutputMode mode = PIO::Stay);
	uint32_t AddLED(LED* pLED);

	Node<LED>* GetHead();

	void Run();

	static void CallBack(void* obj);

private:
	Node<LED>* head;
	uint8_t ledCount;
	//Node<LED>* tail;
};

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* LEDRUNNER_H_ */
