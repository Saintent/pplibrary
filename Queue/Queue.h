/** \file Queue.h
 *	\brief 
 */

#ifndef QUEUE_H_
#define QUEUE_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
#include "stdlib.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
// N/A
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
// N/A
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
#define DEFAULT_QUEUE_MAX_NODE		16
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/*
 *
 */
template <typename T>
class Queue {
public:
	Queue();
	Queue(uint32_t maxItem);
	virtual ~Queue();

	T Peek();
	uint32_t Enqueue(T item);
	T Dequeue();
	uint32_t	Count();

private:

	typedef struct node {
		T	item;
		node* next;
	}node;
	//typedef node* link;

	uint32_t 	size;
	uint32_t 	maxSize;
	node* 		head;
	node*		tail;
};




// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //
//----------- TEMPLATE CLASS DEFINATION---------------------------------------------------------- //
template <typename T>
Queue<T>::Queue() {
	this->head = 0;
	this->tail = 0;
	this->size = 0;
	this->maxSize = DEFAULT_QUEUE_MAX_NODE;

}

template <typename T>
Queue<T>::Queue(uint32_t maxItem) : maxSize(maxItem) {
	this->head = 0;
	this->tail = 0;
	this->size = 0;
}

template <typename T>
Queue<T>::~Queue() {

	while (this->size != 0) {
		this->Dequeue();
	}
}

template <typename T>
T Queue<T>::Peek() {
	T qItem = 0;

	if (this->head != 0) {
		qItem = this->head->item;
	}

	return qItem;
}

template <typename T>
uint32_t Queue<T>::Enqueue(T item) {
	// Create New node
	node*	newNode = 0;
	//node*	nNode;
	uint32_t result = 0;

	if (this->size < this->maxSize) {
		//nNode = new node;
		newNode = (node*) malloc(sizeof(node));
				//molloc(sizeof(node*));
		newNode->item = item;
		newNode->next = 0;

		// if queue is empty
		if (this->size == 0) {
			this->head = newNode;
			this->tail = this->head;
		} else {
			// queue isn't empty
			this->tail->next = newNode;
			this->tail = newNode;
		}

		this->size++;
		result = this->size;
	}

	return result;
}

template <typename T>
T Queue<T>::Dequeue() {
	T qItem = 0;
	node* nextNode;

	if (this->size != 0) {
		qItem = this->Peek();

		// get next node
		nextNode = this->head->next;

		// un allocate head
		delete this->head;

		if (this->head == this->tail) {
			// last item
			this->head = this->tail = 0;

		} else {
			this->head = nextNode;
		}

		this->size--;
	}

	return qItem;
}

template <typename T>
uint32_t Queue<T>::Count() {
	return this->size;
}
#endif /* QUEUE_H_ */
