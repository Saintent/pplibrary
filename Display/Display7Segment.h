/** \file Display7Segment.h
 *	\brief 
 */

#ifndef DISPLAY7SEGMENT_H_
#define DISPLAY7SEGMENT_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "Print.h"
#include "AIO.h"
#include "DisplayBase.h"

// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
#define MAX_DISPLAY_CHAR 	4
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
// N/A
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/*
 *
 */
class Display7Segment: public DisplayBase, public Print {
public:
	Display7Segment(uint8_t clkPin, uint8_t latchPin, uint8_t dataPin);
	virtual ~Display7Segment();

	void begin();
	void Run();

	virtual size_t write(uint8_t);



	void WriteValueToSegment(uint8_t Segment, uint8_t Value);

	using Print::write;

private:
	void show();
	void Align(uint8_t rightJustify =0);
private:
	AIO		clk;
	AIO		latch;
	AIO		data;
	uint8_t displayBuffer[MAX_DISPLAY_CHAR];
	uint8_t displayData[MAX_DISPLAY_CHAR];
	uint8_t bufferIndex;
	uint8_t displayIndex;


};

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* DISPLAY7SEGMENT_H_ */
