/** \file Display7Segment.cpp
 *	\brief 
 */

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
#include "Display7Segment.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PROGRAMMING DEFINITION INCLUDE ----------------------------------------------------- //
// N/A
// ---------- EXTERN OBJECT ---------------------------------------------------------------------- //
// N/A
// ---------- PUBLIC INTERFACE METHOD ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC METHOD (FUNCTION PROTOTYPE) ------------------------------------------------- //
// N/A
// ---------- PUBLIC DATA ----------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE METHOD (FUNCTION PROTOTYPE) ----------------------------------------------- //
// N/A
// ---------- PRIVATE DATA ---------------------------------------------------------------------- //
/* Segment byte maps for numbers 0 to 9 */
const uint8_t SEGMENT_MAP_DIGIT[] = {0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0X80,0X90};

/* Segment byte maps for alpha a-z */
const uint8_t SEGMENT_MAP_ALPHA[] = {136, 131, 167, 161, 134, 142, 144, 139 ,207, 241, 182, 199, 182, 171, 163, 140, 152, 175, 146, 135, 227, 182, 182, 182, 145, 182};

/* Byte maps to select digit 1 to 4 */
const uint8_t SEGMENT_SELECT[] = {0xF1,0xF2,0xF4,0xF8};
const char DISPLAY_OVERFLOW_ERROR = 'E';

// ---------- PRIVATE PROGRAMMING DEFINE -------------------------------------------------------- //
// N/A
// ---------- PRIVATE MACRO DEFINITION ---------------------------------------------------------- //
// N/A
// ---------- SOURCE FILE IMPLEMENTATION -------------------------------------------------------- //

Display7Segment::Display7Segment(uint8_t clkPin, uint8_t latchPin, uint8_t dataPin) {
	// TODO Auto-generated constructor stub
	this->clk.init(clkPin);
	this->latch.init(latchPin);
	this->data.init(dataPin);
	this->displayIndex = 0;
	this->bufferIndex = 0;
}

Display7Segment::~Display7Segment() {
	// TODO Auto-generated destructor stub
}

void Display7Segment::begin() {
	this->clk.Mode(PIO::Output);
	this->latch.Mode(PIO::Output);
	this->data.Mode(PIO::Output);
	memset(this->displayData, 255, MAX_DISPLAY_CHAR);
}

void Display7Segment::Run() {
	this->show();
}

size_t Display7Segment::write(uint8_t ch) {
	uint8_t tempBuffer;
	//Serial.println("Get ch : " + String(ch));
	/*if (this->bufferIndex == 0) {
		Serial.println("Reset Buffer");
		memset(this->displayBuffer, 255, MAX_DISPLAY_CHAR);
	}*/

	if (ch == '\n') {
		this->Align();
		//memcpy(this->displayData, this->displayBuffer. MAX_DISPLAY_CHAR );
		memset(this->displayBuffer, 255, MAX_DISPLAY_CHAR);
		bufferIndex = 0;
	}
	else {
		if (ch <= 10) {
			this->displayBuffer[this->bufferIndex] = SEGMENT_MAP_DIGIT[ch];
			//Serial.println("1-10 Number : " + String(SEGMENT_MAP_DIGIT[ch]) + "  " + String(this->displayBuffer[this->bufferIndex]));
		}
		else if (ch >= '0' && ch <= '9') {
			tempBuffer = ch - '0';
			this->displayBuffer[this->bufferIndex] = SEGMENT_MAP_DIGIT[tempBuffer];
			//Serial.println("1-10 char input : " + String(ch) + "\t" + String(tempBuffer));
		}
		else if ( (ch >= 'a' && ch <= 'z')  ||
				  (ch >= 'A' && ch <= 'Z') ) {
			if (ch >= 'a') {
				tempBuffer = ch & 0xDF;
			}
			else {
				tempBuffer = ch;
			}
			tempBuffer = tempBuffer - 'A';
			this->displayBuffer[this->bufferIndex] = SEGMENT_MAP_ALPHA[tempBuffer];
		}
		else {
			//Serial.println("Get defult : " + String(ch));
			return 1;
		}
		this->bufferIndex = ((this->bufferIndex + 1) % 4);
		//Serial.println("Buffer index : " + String(this->bufferIndex));
	}

	//this->Align();
	return 1;
}

void Display7Segment::Align(uint8_t rightJustify) {
	uint8_t displayTemp[] = {255, 255, 255, 255};
	int8_t idx = MAX_DISPLAY_CHAR - 1;
	uint8_t count = 0;
	/*Serial.println("this->Align();");
	Serial.print("displayBuffer : ");
	for(int i = 0; i < MAX_DISPLAY_CHAR;i++) {

		Serial.print(displayBuffer[i], HEX);
		Serial.print(" ");
	}
	Serial.println();*/
	while (idx >= 0) {
		if (this->displayBuffer[idx] != 0xFF) {
			while (idx >= 0) {
				//Serial.println("New align");
				displayTemp[idx + count] = this->displayBuffer[idx];
				idx--;
			}
			break;
		}
		else {
			count++;
		}
		idx--;
	}
	memcpy(this->displayData, displayTemp, MAX_DISPLAY_CHAR);
	/*Serial.print("displayData : ");
	for(int i = 0; i < MAX_DISPLAY_CHAR;i++) {

		Serial.print(displayData[i], HEX);
		Serial.print(" ");
	}
	Serial.println();*/

}

void Display7Segment::show() {
	this->WriteValueToSegment(this->displayIndex, this->displayData[this->displayIndex]);
	this->displayIndex = (this->displayIndex + 1) % 4;
}

void Display7Segment::WriteValueToSegment(uint8_t segment, uint8_t value) {
	uint8_t i;

	latch.Write(PIO::Low);

	for (i = 0; i < 8; i++) {
		data.Write(!!(value & (1 << (7 - i))));
		clk.Write(PIO::High);
		clk.Write(PIO::Low);
	}

	for (i = 0; i < 8; i++) {
		data.Write(!!(SEGMENT_SELECT[segment] & (1 << (7 - i))));
		clk.Write(PIO::High);
		clk.Write(PIO::Low);
	}

	latch.Write(PIO::High);

}

//=========== Public Method ======================================================================//

//=========== Private Method ======================================================================//

// ---------- END OF SOURCE FILE IMPLEMENTATION ------------------------------------------------- //
