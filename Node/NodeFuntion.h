/** \file NodeFuntion.h
 *  \brief template of node function definition
 */

#ifndef NODEFUNTION_H_
#define NODEFUNTION_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "Node.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
//
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
//
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //
/** \brief Add new node to last node
 * 	\param data data of new node
 * 	\param head Pointer to pointer of list entry
 * 	\return Node id
 */
template <class T>
uint32_t NodeAdd(T* data, Node<T>** head) {
	Node<T>* newNode = new Node<T>(data, 0);
	Node<T>* currentNode;
	uint32_t nodeId = 0;

	if (head[0] == 0) {
		head[0] = newNode;
		newNode->id = 0;
	} else {
		currentNode = head[0];
		while (currentNode) {
			nodeId++;
			if (currentNode->NextNode() == 0) {
				newNode->id = nodeId;
				currentNode->InsertAfter(newNode);
				break;
			} else {
				currentNode = currentNode->NextNode();

			}
		}
	}

	return nodeId;
}

/** \brief Get description of node by id
 * 	\param head Pointer to start entry
 * 	\param id key to search
 * 	\return Pointer of node
 * 			0 is node isn't exit
 */
template <class T>
Node<T>* NodeGet(Node<T>* head, uint32_t id) {
	Node<T>* returnNode = 0;
	Node<T>* currentNode;

	currentNode = head;

	while (currentNode) {
		if (currentNode->id == id) {
			returnNode = currentNode;
			break;
		} else {
			currentNode = currentNode->NextNode();

		}
	}
	return returnNode;
}



// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* NODEFUNTION_H_ */
