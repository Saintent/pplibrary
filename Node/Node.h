/**\file node.h
 * \brief Node class template definition
 */

#ifndef NODE_H_
#define NODE_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
//
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
//
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/*	\brief node template class
 *
 */
template <class T>
class Node {
public:
	Node() {
		id = 0;
		next = 0;
		data = 0;
	};

	Node(T* item, Node<T>* next = 0) {
	    this->data = item;
	    this->next = next;
	    id = 0;
	};

	virtual ~Node() { };

	uint32_t id;		//!< 	Id of this node
	T* data;			//!<	Data of node

	/** \brief Get pointer of next node
	 * 	\return Pointer of next node
	 */
    Node<T>* NextNode() {
    	return this->next;
    };

    /**	\brief Add next node into next pointer node
     * 	\param p Pointer of next node
     */
    void InsertAfter(Node<T>* p) {

		// list to the Node<T>* p first
		p->next = this->next;

		//inserting after,
		this->next = p;
    };

    /** \brief Delete next node
     * 	\return Pointer of next node (just pointer the object still be allocate.
     * 			Recommend use "delete" to free allocated memory
     */
    Node<T>* DeleteAfter() {
        // store the next Node in a temporary Node

        Node<T>* tempNode = next;
        // check if there is a next node
        if(next != 0) {
            next = next->next;
        }

        return tempNode;
    };
    //Node<T> * GetNode(const T& item, Node<T>* next = 0);
private:

    Node<T> * next;			//!< Next node pointer

};

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* NODE_H_ */
