/**\file TaskBase.h
 * \brief TaskBase class
 */

#ifndef TASKBASE_H_
#define TASKBASE_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //

#include "TaskDef.h"
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
//#define TASK_PRE_ALLOCATE 1

#ifndef TASK_PRE_ALLOCATE
#define TASK_PRE_ALLOCATE	0
#endif
#ifndef TASK_MAX
#define TASK_MAX		8
#endif
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
//
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/**	\class TaskBase
 *	\brief Base class for other Task class
 */
class TaskBase {
public:
	TaskBase();
	virtual ~TaskBase();

	/** \fn virtual void begin()
	 * \brief Abstract function for initialized class
	 */
	virtual void begin() = 0;

	void Run();
	uint8_t Start(uint8_t  id);

	uint8_t AddTask(TaskFunction* fn, void* argv , uint32_t period, Task::TaskMode mode);
	taskNode_t* GetTaskNode(uint8_t id);
	uint8_t DeleteTask(uint8_t id);
	uint8_t	ActiveTask(uint8_t id);

private:
	/**	\fn virtual uint32_t GetTick()
	 * 	\brief abstract function to get current time
	 */
	virtual uint32_t GetTick() = 0;


private:
	taskNode_t*		headNode;					//!< Entry of link list
#if TASK_PRE_ALLOCATE==1
	taskNode_t		handleTask[TASK_MAX];		//!< Pre-allocate
#endif
	uint8_t			taskCount;					//!< Task handler count
	//uint8_t			tickTime;					//!< Period time can be execute

};

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* TASKBASE_H_ */
