/*
 * SoftwareTimerTask.h
 *
 *  Created on: Sep 30, 2016
 *      Author: Prustya
 */

#ifndef SOFTWARETIMERTASK_H_
#define SOFTWARETIMERTASK_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "TaskBase.h"

// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
//
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
//
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/*
 *
 */
class SoftwareTimerTask: public TaskBase {
public:
	SoftwareTimerTask();
	virtual ~SoftwareTimerTask();

	void begin();

private:
	uint32_t GetTick();
};

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* SOFTWARETIMERTASK_H_ */
