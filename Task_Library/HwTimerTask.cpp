/*
 * HwTimerTask.cpp
 *
 *  Created on: Oct 1, 2016
 *      Author: Prustya
 */

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include <avr/sleep.h>

#if ARDUINO >= 100
#include "Arduino.h"
#endif


#include <HwTimerTask.h>

// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PROGRAMMING DEFINITION INCLUDE ----------------------------------------------------- //
// N/A
// ---------- EXTERN OBJECT ---------------------------------------------------------------------- //
// N/A
// ---------- PUBLIC INTERFACE METHOD ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC METHOD (FUNCTION PROTOTYPE) ------------------------------------------------- //
// N/A
// ---------- PUBLIC DATA ----------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE METHOD (FUNCTION PROTOTYPE) ----------------------------------------------- //
// N/A
// ---------- PRIVATE DATA ---------------------------------------------------------------------- //
#define SYS_CLK		16000000UL
#define TM_PR_CLK	8UL
// ---------- PRIVATE PROGRAMMING DEFINE -------------------------------------------------------- //


// ---------- PRIVATE MACRO DEFINITION ---------------------------------------------------------- //
// N/A
// ---------- SOURCE FILE IMPLEMENTATION -------------------------------------------------------- //
#ifndef IMPLEMENT_ISR
HwTimerTask hwTask;
#endif

HwTimerTask::HwTimerTask() {
	// TODO Auto-generated constructor stub
	tick = 0;
}

HwTimerTask::~HwTimerTask() {
	// TODO Auto-generated destructor stub
}

void HwTimerTask::begin() {
	begin(HwTimer::Period_1ms);
}

void HwTimerTask::begin(HwTimer::HwPeriod period) {

	uint16_t ocrVal;
	uint32_t scale;
	uint32_t tmClk;

	tmClk = SYS_CLK/TM_PR_CLK;
	scale = (uint32_t)1000000000/tmClk;

	ocrVal = (uint16_t)(((uint32_t)period * 1000) / scale) - 1;

	// Time
	cli(); //disable global interrupt
	TCCR1A = 0;
	TCCR1B = 1 << 3 | 1 << 1;
	TCNT1 = 0;
	OCR1A = ocrVal;		//  resolution : 500ns (1/2e+6)
	TIMSK1 = (1 << OCIE1A);

	sei();  //enable global interrupt
}

void HwTimerTask::HwTimerCallBack(HwTimerTask* obj) {
	obj->tick++;
}

uint32_t HwTimerTask::GetTick() {
	return this->tick;
}

#ifndef IMPLEMENT_ISR
ISR (TIMER1_COMPA_vect) {
	HwTimerTask::HwTimerCallBack(&hwTask);
}
#endif

//=========== Public Method ======================================================================//

//=========== Private Method ======================================================================//

// ---------- END OF SOURCE FILE IMPLEMENTATION ------------------------------------------------- //
