/**\file TaskDef.h
 * \brief define structure of task
 */
#ifndef TASKDEF_H_
#define TASKDEF_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
//
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
namespace Task  {
	//! TaskState enum.
	/*! State of current task*/
    enum TaskState 	{
        Stop,			//!< Stop State
        Run,			//!< Run State
        Pause,			//!< Pause State
	};
	//! TaskMode enum.
	/*! Mode of current task*/
    enum TaskMode {
    	Period,			//!< Period Mode run repeat
		OneTime			//!< One time Mode run only one time per activated
    };
};
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
/**\var typedef void TaskFunction(void* obj)
 * \brief Call back function to execute
 * \param obj Pointer to object to call back
 *
 */
typedef void TaskFunction(void* obj);
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
/**
 * \struct taskData_t
 * \brief contain variable of task
 * Size : 20 byte
 */
typedef __attribute__((aligned(4)))  struct taskData_tag {
	TaskFunction* 	callFunction;	//!< Call function
	void*			argv;			//!< Argument to call back
	uint32_t 		period;			//!< Period of task
	uint32_t		nextTick;		//!< Time to next tick
	Task::TaskState	state;			//!< Current State
	Task::TaskMode	mode;			//!< Current Mode
	uint8_t			waitForExe;		//!< Active flag (use for active task immediately)
	uint8_t 		id;				//!< Id of task
}taskData_t;

/**
 * \struct taskNode_t
 * \brief contain data of node
 */
typedef __attribute__((aligned(4))) struct taskNode_tag {
	taskData_t		task;
	taskNode_tag*	nextNode;
}taskNode_t;

// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //


// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* TASKDEF_H_ */
