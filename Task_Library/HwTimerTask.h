/*
 * HwTimerTask.h
 *
 *  Created on: Oct 1, 2016
 *      Author: Prustya
 */

#ifndef HWTIMERTASK_H_
#define HWTIMERTASK_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include <TaskBase.h>

// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
#define IMPLEMENT_ISR
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
namespace HwTimer {
	enum HwPeriod{
		Period_10us = 10,
		Period_100us = 100,
		Period_1ms = 1000
	};
}
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/*
 *
 */
class HwTimerTask: public TaskBase {
public:
	HwTimerTask();
	virtual ~HwTimerTask();

	void begin();
	void begin(HwTimer::HwPeriod period = HwTimer::Period_1ms);

	static void HwTimerCallBack(HwTimerTask* obj);
private:
	uint32_t GetTick();
	uint32_t tick;
};
#ifndef IMPLEMENT_ISR
extern HwTimerTask hwTask;
#endif

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* HWTIMERTASK_H_ */
