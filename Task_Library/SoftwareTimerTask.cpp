/*
 * SoftwareTimerTask.cpp
 *
 *  Created on: Sep 30, 2016
 *      Author: Prustya
 */

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "SoftwareTimerTask.h"
#ifdef ARDUINO >= 100
#include <Arduino.h>
#endif
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PROGRAMMING DEFINITION INCLUDE ----------------------------------------------------- //
// N/A
// ---------- EXTERN OBJECT ---------------------------------------------------------------------- //
// N/A
// ---------- PUBLIC INTERFACE METHOD ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC METHOD (FUNCTION PROTOTYPE) ------------------------------------------------- //
// N/A
// ---------- PUBLIC DATA ----------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE METHOD (FUNCTION PROTOTYPE) ----------------------------------------------- //
// N/A
// ---------- PRIVATE DATA ---------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE PROGRAMMING DEFINE -------------------------------------------------------- //
// N/A
// ---------- PRIVATE MACRO DEFINITION ---------------------------------------------------------- //
// N/A
// ---------- SOURCE FILE IMPLEMENTATION -------------------------------------------------------- //

SoftwareTimerTask::SoftwareTimerTask() {
	// TODO Auto-generated constructor stub

}

SoftwareTimerTask::~SoftwareTimerTask() {
	// TODO Auto-generated destructor stub
}

void SoftwareTimerTask::begin() {

}

uint32_t SoftwareTimerTask::GetTick() {
	return micros();
}

//=========== Public Method ======================================================================//

//=========== Private Method ======================================================================//

// ---------- END OF SOURCE FILE IMPLEMENTATION ------------------------------------------------- //
