/**\file TaskBase.cpp
 * \brief TaskBase class
 */

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "../Task_Library/TaskBase.h"

#include "stdlib.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PROGRAMMING DEFINITION INCLUDE ----------------------------------------------------- //
// N/A
// ---------- EXTERN OBJECT ---------------------------------------------------------------------- //
// N/A
// ---------- PUBLIC INTERFACE METHOD ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC METHOD (FUNCTION PROTOTYPE) ------------------------------------------------- //
// N/A
// ---------- PUBLIC DATA ----------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE METHOD (FUNCTION PROTOTYPE) ----------------------------------------------- //
// N/A
// ---------- PRIVATE DATA ---------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE PROGRAMMING DEFINE -------------------------------------------------------- //
#define TIME_MASK 		0xFFFFFFFFUL
// ---------- PRIVATE MACRO DEFINITION ---------------------------------------------------------- //
// N/A
// ---------- SOURCE FILE IMPLEMENTATION -------------------------------------------------------- //

TaskBase::TaskBase() : taskCount(0) , headNode(0) {

}

TaskBase::~TaskBase() {

}

/** \fn void TaskBase::Run(void)
 * \brief Main task to process register task
 */
void TaskBase::Run(void) {
	taskNode_t* node;
	taskData_t* task;
	uint32_t currentTime;
	uint8_t nodeCount;

	node = headNode;
	currentTime = GetTick() & TIME_MASK;

	nodeCount = taskCount;

	// Traverse
	do {
		task = &node->task;
		if ( (currentTime >= task->nextTick) &&
				(task->state == Task::Run) ) {
			task->waitForExe = 1;
			// Calculate next tick;
			task->nextTick = (currentTime + task->period) & TIME_MASK;
		}

		if (task->waitForExe == 1) {
			if (task->callFunction != 0) {
				// execute
				task->callFunction(task->argv);
			}
			task->waitForExe = 0;
		}
		// Read next node
		node = node->nextNode;
		nodeCount--;
	}
	while(nodeCount != 0);
}

/** \fn uint8_t TaskBase::Start(uint8_t id)
 * \brief Start task
 * \param id Task id to start
 * \return result 0 is success
 */
uint8_t TaskBase::Start(uint8_t id) {
	taskNode_t* node;
	uint8_t result = 0;

	node = GetTaskNode(id);

	if (node != 0) {
		node->task.state = Task::Run;
	}
	else {
		result = 255;
	}

	return result;
}

/** \fn uint8_t TaskBase::AddTask(TaskFunction* fn, void* argv = 0,
		uint32_t period = 1000,
		task::TaskMode mode = task::Period)
 * \brief Add task to process
 * \param  fn Pointer function to execute
 * \param argv Call back object to execute
 * \param period Value of period time to execute
 * \param mode Mode of this task @see TaskMode
 * \return Task id was registered
 */
uint8_t TaskBase::AddTask(TaskFunction* fn, void* argv = 0,
		uint32_t period = 1000,
		Task::TaskMode mode = Task::Period) {
	taskNode_t* node;
	//taskNode_t* entry;
	//uint8_t returnValue;

	if (taskCount < TASK_MAX) {

		node = (taskNode_t*) malloc(sizeof(taskNode_t));

		// Initialized node
		node->task.callFunction = fn;
		node->task.argv = argv;
		node->task.mode = mode;
		node->task.state = Task::Run;
		node->task.period = period;
		node->task.nextTick = (GetTick() + period) & TIME_MASK;
		node->task.waitForExe = 0;
		node->task.id = taskCount;


		if (headNode == 0) {
			// First entry
			node->nextNode = 0;
		}
		else {
			// Insert new head
			node->nextNode = headNode;
		}

		// set new head
		headNode = node;

		// Increment Handler task
		taskCount++;
	}

	return node->task.id;
}

/** \fn taskNode_t* TaskBase::GetTaskNode(uint8_t id)
 * \brief Get node description
 * \param id Task id to get description
 * \return Pointer to node
 */
taskNode_t* TaskBase::GetTaskNode(uint8_t id) {
	taskNode_t* node;
	taskNode_t* returnNode;
	uint8_t count;

	count = 0;
	node = headNode;

	while(count < TASK_MAX) {
		if(node->task.id == id) {
			returnNode = node;
			break;
		}
		node++;
		count++;
	}

	return returnNode;
}

/** \fn uint8_t TaskBase::DeleteTask(uint8_t id)
 * 	\brief Delete task by task id
 * 	\param id task id to deleted
 * 	\return result 0 is success
 */

uint8_t TaskBase::DeleteTask(uint8_t id) {
	taskNode_t* nodeDel = 0;
	taskNode_t* node;
	uint8_t count = 0;
	uint8_t result = 0;

	node = headNode;

	while(count < TASK_MAX) {
		// Search entry
		if (node->nextNode->task.id == id) {
			nodeDel = node->nextNode;
			break;
		}
		// last entry
		else if (node->nextNode == 0) {
			break;
		}
		node++;
		count++;
	}

	if (nodeDel != 0) {
		// Node is exit
		// Set new pointer to new node
		node->nextNode = nodeDel->nextNode;
		// free memory of delete node
		free(nodeDel);
	}
	else {
		// Node is not exit
		result = 255;
	}

	return result;
}

/** \fn uint8_t TaskBase::ActiveTask(uint8_t id)
 * 	\brief Execute task by task id
 * 	\param id task id to execute
 * 	\return result 0 is success
 */
uint8_t TaskBase::ActiveTask(uint8_t id) {
	taskNode_t* node;
	uint8_t result = 0;

	node = GetTaskNode(id);

	if (node != 0) {
		node->task.waitForExe = 1;
	}
	else {
		result = 255;
	}

	return result;

}
//=========== Public Method ======================================================================//

//=========== Private Method ======================================================================//

// ---------- END OF SOURCE FILE IMPLEMENTATION ------------------------------------------------- //
