/**\file IOBase.h
 * \brief Abstract io class
 */
#ifndef IOBASE_H_
#define IOBASE_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
//
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
namespace PIO {
	//!< IO state enum.
	//!< Represent to current state of input output
	enum IOState {
		Low,		//!< Low state ( 0V )
		High		//!< High state ( ex. 5V , 3.3V )
	};

	//!< IO mode enum.
	//!< Represent to current mode of pin
	enum IOMode {
		Input,		//!< Input mode
		Output		//!< Output mode
	};

	//!< IO output mode
	//!< Represent to current output mode
	enum IOOutputMode {
		Stay,		//!< Stay mode (not support now)
		OneShot,	//!< One pulse mode
		Toggle,		//!< Toggle mode
		PWM			//!< Pulse width modulation mode
	};
}
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //
namespace PIO {

/*
 *
 */
class IOBase {
public:
	IOBase();								//!< Class constructor
	virtual ~IOBase();						//!< Class destructor

	virtual uint8_t Read() = 0;				//!< abstract read function
	virtual uint8_t Write(IOState) = 0;		//!< abstract write function
	virtual uint8_t Mode(IOMode) = 0;		//!< abstract mode function
protected:
	uint8_t pin;							//!< IO location
	IOState state;							//!< IO State
};

} /* namespace PIO */
// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* IOBASE_H_ */
