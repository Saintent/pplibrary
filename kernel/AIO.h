/** \file AIO.h
 *  \brief AIO class use for Arduino platform
 */

#ifndef AIO_H_
#define AIO_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
#include "IOBase.h"
#ifdef ARDUINO >= 100
#include <Arduino.h>
#endif
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
//
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
//
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/* \brief IO interface class base on Arduino platform
 *
 */
class AIO : public PIO::IOBase {
public:
	/** \brief Class constructor
	 *
	 */
	AIO();

	/**	\brief Class destructor
	 *
	 */
	virtual ~AIO();

	/** \brief Read pin function
	 *  \param pin location of pin
	 */
	void init(uint8_t pin);

	/** \brief Read pin function
	 *  \return Result of read pin
	 */
	uint8_t Read() { return digitalRead(pin); }

	/** \brief Write value to pin
	 * 	\param state Write value
	 * 	\return Process result
	 */
	uint8_t Write(PIO::IOState state) {
		this->state = state;
		digitalWrite(pin, state == PIO::High ? HIGH : LOW);
		return true;
	}

	/** \brief Write value to pin
	 * 	\param state Write value
	 * 	\return Process result
	 */
	uint8_t Write(uint8_t state) {
		this->state = state;
		digitalWrite(pin, state > 0 ? HIGH : LOW);
		return true;
	}


	/** \brief Set pin mode
	 * 	\param mode Mode to set
	 * 	\return Process result
	 */
	uint8_t Mode(PIO::IOMode mode) {
		pinMode(pin, mode == PIO::Input ? INPUT : OUTPUT);
		return true;
	}

	/** \brief Set pin mode
	 * 	\param mode Mode to set
	 * 	\return Process result
	 */
	uint8_t Mode(uint8_t mode) {
		pinMode(pin, mode);
		return true;
	}

	/** \brief Get current state of pin
	 *  \return Current state
	 */
	PIO::IOState GetState() {
		return state;
	}
};

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* AIO_H_ */
