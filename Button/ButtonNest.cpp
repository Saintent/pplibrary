/** \file ButtonNest.cpp
 *	\brief 
 */

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "ButtonNest.h"
#include "NodeFuntion.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PROGRAMMING DEFINITION INCLUDE ----------------------------------------------------- //
// N/A
// ---------- EXTERN OBJECT ---------------------------------------------------------------------- //
// N/A
// ---------- PUBLIC INTERFACE METHOD ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC METHOD (FUNCTION PROTOTYPE) ------------------------------------------------- //
// N/A
// ---------- PUBLIC DATA ----------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE METHOD (FUNCTION PROTOTYPE) ----------------------------------------------- //
// N/A
// ---------- PRIVATE DATA ---------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE PROGRAMMING DEFINE -------------------------------------------------------- //
// N/A
// ---------- PRIVATE MACRO DEFINITION ---------------------------------------------------------- //
// N/A
// ---------- SOURCE FILE IMPLEMENTATION -------------------------------------------------------- //

ButtonNest::ButtonNest() {
	// TODO Auto-generated constructor stub
	this->head = 0;
	this->buttonCount = 0;

}

ButtonNest::~ButtonNest() {
	// TODO Auto-generated destructor stub
}

uint32_t ButtonNest::AddButton(uint8_t id, uint8_t pin, void (*callBack)(Button*, BUTTON::ButtonEvent)) {
	Button* newButton;

	newButton = new Button(id, pin, callBack);
	buttonCount++;
	return NodeAdd<Button>(newButton, &this->head);
}

uint32_t ButtonNest::AddButton(Button* pButton) {
	buttonCount++;
	return NodeAdd<Button>(pButton, &this->head);
}

Node<Button>* ButtonNest::GetHead() {
	return this->head;
}

void ButtonNest::Run() {
	Node<Button>* node;
	Button* bt;
	node = this->head;

	while (node) {
		node->data->Process();
		node = node->NextNode();
	}
}
//=========== Public Method ======================================================================//

//=========== Private Method ======================================================================//

// ---------- END OF SOURCE FILE IMPLEMENTATION ------------------------------------------------- //
