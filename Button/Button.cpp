/** \file Button.cpp
 *	\brief 
 */

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "IOBase.h"
#include <Button.h>
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PROGRAMMING DEFINITION INCLUDE ----------------------------------------------------- //
// N/A
// ---------- EXTERN OBJECT ---------------------------------------------------------------------- //
// N/A
// ---------- PUBLIC INTERFACE METHOD ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC METHOD (FUNCTION PROTOTYPE) ------------------------------------------------- //
// N/A
// ---------- PUBLIC DATA ----------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE METHOD (FUNCTION PROTOTYPE) ----------------------------------------------- //
// N/A
// ---------- PRIVATE DATA ---------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE PROGRAMMING DEFINE -------------------------------------------------------- //
// N/A
// ---------- PRIVATE MACRO DEFINITION ---------------------------------------------------------- //
// N/A
// ---------- SOURCE FILE IMPLEMENTATION -------------------------------------------------------- //

Button::Button() {
	// TODO Auto-generated constructor stub
	this->Init(0);
}

Button::Button(uint8_t id, uint8_t pin, void (*cb)(Button*, BUTTON::ButtonEvent)) {
	this->Init(id, pin, cb);
}

Button::~Button() {
	// TODO Auto-generated destructor stub
}

void Button::Init(uint8_t id, uint8_t pin, void (*cb)(Button*, BUTTON::ButtonEvent)) {
	this->pin = pin;
	this->Id = id;
	this->callBack = cb;
	this->currentState = BUTTON::ButtonStateIdle;
	this->lastState = this->currentState;
	//this->debounceTime = 50;
	//this->longPressTime = 2000;
	this->longPressDecisionTime = 0;
	//this->longPressCallbackTime = 200;
	this->clickDecisionTime = 0;
	this->timeCounter = 0;
	this->click = 0;
	this->activeOn = PIO::Low;
	this->Mode(PIO::Input);
}

void Button::SetActiveOn(PIO::IOState state) {
	this->activeOn = state;
}

void Button::Process() {

	this->state = this->Read();

	switch (this->currentState) {
	case BUTTON::ButtonStateIdle :

		if (this->state == this->activeOn) {

			if(++this->timeCounter >= BUTTON_DEBOUNCE_TIME) {
				this->lastState = this->currentState;
				this->currentState = BUTTON::ButtonStatePush;
				this->timeCounter = 0;

				if (this->callBack != 0) {
					this->callBack(this, BUTTON::ButtonEventPush);
				}

			}
		}
		else {
			this->timeCounter = 0;
		}

		break;
	case BUTTON::ButtonStatePush :

		if (this->state != this->activeOn) {

			if (++this->timeCounter >= BUTTON_DEBOUNCE_TIME) {
				this->lastState = this->currentState;
				this->currentState = BUTTON::ButtonStateRelease;
				this->timeCounter = 0;
			}

		}
		else {

			if (++this->longPressDecisionTime >= BUTTON_LONG_PRESS_TIME) {
				this->lastState = this->currentState;
				this->currentState = BUTTON::ButtonStateLongPush;
				this->longPressDecisionTime = 0;
				this->cbTimeCounter = 0;

				if (this->callBack != 0) {
					this->callBack(this, BUTTON::ButtonEventLongClick);
				}

			}
			this->timeCounter = 0;
		}
		break;
	case BUTTON::ButtonStateLongPush :

		if (this->state != this->activeOn) {

			if (++this->timeCounter >= BUTTON_DEBOUNCE_TIME) {
				this->lastState = this->currentState;
				this->currentState = BUTTON::ButtonStateRelease;
				this->timeCounter = 0;
				this->cbTimeCounter = 0;
			}

		}
		else {

			if (++this->cbTimeCounter >= BUTTON_LONG_PRESS_CALL) {

				if (this->callBack != 0) {
					this->callBack(this, BUTTON::ButtonEventLongClick);
				}
				this->cbTimeCounter = 0;
			}

			this->timeCounter = 0;
		}

		break;
	case BUTTON::ButtonStateRelease :
		if (this->lastState != BUTTON::ButtonStateLongPush) {
			this->click++;

		}

		if (this->callBack != 0) {
			this->callBack(this, BUTTON::ButtonEventRelease);
		}

		this->lastState = this->currentState;
		this->currentState = BUTTON::ButtonStateIdle;
		break;
	}

	if (this->click) {

		if (++this->clickDecisionTime >= BUTTON_CLIKC_TIME) {
			if (this->click == 1) {
				if (this->callBack != 0) {
					this->callBack(this, BUTTON::ButtonEventClick);
				}
			}
			else {
				if (this->callBack != 0) {
					this->callBack(this, BUTTON::ButtonEventDoubleClick);
				}
			}
			this->click = 0;
			this->clickDecisionTime = 0;

		}
	}

}

//=========== Public Method ======================================================================//

//=========== Private Method ======================================================================//

// ---------- END OF SOURCE FILE IMPLEMENTATION ------------------------------------------------- //
