/** \file Button.h
 *	\brief 
 */

#ifndef BUTTON_H_
#define BUTTON_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
#include <AIO.h>

// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
#define BUTTON_DEBOUNCE_TIME		50
#define BUTTON_LONG_PRESS_TIME		2000
#define BUTTON_LONG_PRESS_CALL		200
#define BUTTON_CLIKC_TIME			300
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
namespace BUTTON {
	enum ButtonState {
		ButtonStateIdle,
		ButtonStatePush,
		ButtonStateLongPush,
		ButtonStateRelease
	};

	enum ButtonEvent {
		ButtonEventPush,
		ButtonEventClick,
		ButtonEventDoubleClick,
		ButtonEventLongClick,
		ButtonEventRelease
	};
}
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //

//void ButtonCallbackFunction(T obj, BUTTON::ButtonEvent evt);
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/*
 *
 */
class Button: public AIO {
public:
	Button();
	Button(uint8_t id, uint8_t pin, void (*cb)(Button*, BUTTON::ButtonEvent) = 0);
	virtual ~Button();

	void Init(uint8_t id, uint8_t pin = 0, void (*cb)(Button*, BUTTON::ButtonEvent) = 0);
	void SetActiveOn(PIO::IOState state);

	void Process(void);


public:
	uint8_t Id;
	void (*callBack)(Button* obj, BUTTON::ButtonEvent evt);
private:


	BUTTON::ButtonState currentState;
	BUTTON::ButtonState lastState;
	PIO::IOState activeOn;

	//uint32_t debounceTime;
	//uint32_t longPressTime;
	//uint32_t longPressCallbackTime;
	uint32_t longPressDecisionTime;
	uint32_t clickDecisionTime;
	uint32_t timeCounter;
	uint32_t cbTimeCounter;
	uint32_t click;

};

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* BUTTON_H_ */
