/** \file ButtonNest.h
 *	\brief 
 */

#ifndef BUTTONNEST_H_
#define BUTTONNEST_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
#include "IOBase.h"
#include "Node.h"
#include "Button.h"
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
// N/A
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
// N/A
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/*
 *
 */
class ButtonNest {
public:
	ButtonNest();
	virtual ~ButtonNest();

	uint32_t AddButton(uint8_t id, uint8_t pin, void (*callBack)(Button*, BUTTON::ButtonEvent));
	uint32_t AddButton(Button* pButton);

	Node<Button>* GetHead();

	void Run();

private:
	Node<Button>* head;
	uint8_t buttonCount;
	//Node<Button>* tail;
};

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* BUTTONNEST_H_ */
