/** \file OBDDisplay.cpp
 *	\brief 
 */

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
//
#include "OBDDisplay.h"
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PROGRAMMING DEFINITION INCLUDE ----------------------------------------------------- //
// N/A
// ---------- EXTERN OBJECT ---------------------------------------------------------------------- //
// N/A
// ---------- PUBLIC INTERFACE METHOD ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC METHOD (FUNCTION PROTOTYPE) ------------------------------------------------- //
// N/A
// ---------- PUBLIC DATA ----------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE METHOD (FUNCTION PROTOTYPE) ----------------------------------------------- //
// N/A
// ---------- PRIVATE DATA ---------------------------------------------------------------------- //
// N/A
// ---------- PRIVATE PROGRAMMING DEFINE -------------------------------------------------------- //
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF
// ---------- PRIVATE MACRO DEFINITION ---------------------------------------------------------- //
// N/A
// ---------- SOURCE FILE IMPLEMENTATION -------------------------------------------------------- //

OBDDisplay::OBDDisplay() {
	// TODO Auto-generated constructor stub

}

OBDDisplay::~OBDDisplay() {
	// TODO Auto-generated destructor stub
}

void OBDDisplay::DisplaySetup() {

	if (display == 0) {
		return;
	}

	display->begin(0x9341);

	display->setRotation(2); // Need for the Mega, please changed for your choice or rotation initial

	display->fillScreen(BLACK);
	// Speed Section
	display->drawRect(0, 0, 180, 160, RED);
	display->setCursor(10, 10);
	display->setTextSize(4);
	display->setTextColor(WHITE);
	display->println("Speed");

	// Current Speed
	display->setCursor(20, 60);
	display->setTextSize(8);
	display->print("120");

	// Unit
	display->setCursor(100, 130);
	display->setTextSize(3);
	display->println("km/h");


	// Throttle Section
	display->drawRect(180, 0, 60, 160, RED);
	display->setCursor(185, 10);
	display->setTextSize(1);
	display->println("Throttle");


	display->setCursor(185, 140);
	display->setTextSize(2);
	display->println("80 %");

	// RPM Section
	display->drawRect(0, 160, 240, 90, RED);

	display->setCursor(70, 205);
	display->setTextSize(5);
	display->print("5000");

	display->setCursor(195, 230);
	display->setTextSize(2);
	display->print("rpm");

	// Other Section
	display->drawRect(0, 250, 240, 70, RED);
}

void OBDDisplay::UpdateSpeed(uint16_t spd) {
	if (this->display == 0) {
		return;
	}
	// Clean up
	display->fillRect(20, 60, 144, 64, BLACK);
	display->setCursor(20, 60);
	display->setTextSize(8);
	display->print(String(spd));
}

//=========== Public Method ======================================================================//

//=========== Private Method ======================================================================//

// ---------- END OF SOURCE FILE IMPLEMENTATION ------------------------------------------------- //
