/** \file OBDDisplay.h
 *	\brief 
 */

#ifndef OBDDISPLAY_H_
#define OBDDISPLAY_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
#include "stdint.h"
#include <SPFD5408_Adafruit_GFX.h>    // Core graphics library
#include <SPFD5408_Adafruit_TFTLCD.h> // Hardware-specific library
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
// N/A
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
// N/A
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //

/*
 *
 */
class OBDDisplay {
public:
	OBDDisplay();
	virtual ~OBDDisplay();

	void Init(Adafruit_TFTLCD* display);
	void DisplaySetup(void);
	void UpdateSpeed(uint16_t spd);

private:
	Adafruit_TFTLCD* display;
};

// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* OBDDISPLAY_H_ */
