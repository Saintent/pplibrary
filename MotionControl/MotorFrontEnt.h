/** \file MotorFrontEnt.h
 *	\brief 
 */

#ifndef MOTORFRONTENT_H_
#define MOTORFRONTENT_H_

// ---------- SYSTEM INCLUDE --------------------------------------------------------------------- //
//
// ---------- EXTERNAL MODULE INCLUDE ------------------------------------------------------------ //
// N/A
// ---------- PUBLIC PROGRAMMING DEFINE ---------------------------------------------------------- //
// N/A
// ---------- ENUMERATOR DEFINITION -------------------------------------------------------------- //
// N/A
// ---------- TYPEDEF DATA TYPE DEFINITION ------------------------------------------------------- //
// N/A
// ---------- STRUCT OR UNION DATA TYPE DEFINITION ----------------------------------------------- //
// N/A
// ---------- PUBLIC MACRO DEFINITION ------------------------------------------------------------ //
// N/A
// ---------- EXTERN FUNCTION -------------------------------------------------------------------- //
// N/A
// ---------- EXTERN VARIABLE -------------------------------------------------------------------- //
// N/A
// ---------- CLASS DECLARATION ----------------------------------------------------------------- //
namespace MOTOR {

/*
 *
 */
class MotorFrontEnt {
public:
	MotorFrontEnt();
	virtual ~MotorFrontEnt();
};

} /* namespace MOTOR */
// ---------- END OF CLASS DECLARATION ---------------------------------------------------------- //

#endif /* MOTORFRONTENT_H_ */
